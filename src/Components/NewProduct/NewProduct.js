import React, {Component, Fragment} from 'react';
import {PageHeader} from "react-bootstrap";
import {connect} from "react-redux";
import ProductForm from "../../Containers/ProductForm/ProductForm";
import {createProduct} from "../../store/actions/products";

class NewPost extends Component {

  createProduct = productData => {
    const token = this.props.user.token;

    this.props.onCreateProduct(productData, token);
  };

  render() {
    return(
      <Fragment>
        <PageHeader>Create new product</PageHeader>
        <ProductForm user={this.props.user} onSubmit={this.createProduct}/>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  onCreateProduct: (productData, token) => dispatch(createProduct(productData, token))
});

export default connect(mapStateToProps, mapDispatchToProps)(NewPost);