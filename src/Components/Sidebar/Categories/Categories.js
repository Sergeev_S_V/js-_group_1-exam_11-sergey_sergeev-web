import React, {Component} from 'react';
import categories from '../../../categories';
import {Nav, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

class Categories extends Component {

  firstLetterUppercase = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };

  renderCategories = (categories) => {
    return categories.map(category => (
      <LinkContainer key={category}
                     to={`/products/${category}`}>
        <NavItem>
          {this.firstLetterUppercase(category)}
        </NavItem>
      </LinkContainer>
    ));
  };
  render() {
    return(
      <Nav>
        {this.renderCategories(categories)}
      </Nav>
    );
  }
}



export default Categories;