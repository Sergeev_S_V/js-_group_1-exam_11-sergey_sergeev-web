import axios from '../../axios-api';
import {
  CREATE_PRODUCT_SUCCESS, DELETE_PRODUCT_SUCCESS, FETCH_ALL_PRODUCTS_SUCCESS,
  GET_PRODUCT_SUCCESS
} from "./actionTypes";
import {push} from "react-router-redux";

export const fetchProductsSuccess = products => {
  return {type: FETCH_ALL_PRODUCTS_SUCCESS, products};
};

export const fetchProducts = (category) => dispatch => {
  if (category) {
    axios.get(`/products?category=${category}`).then(
      response => dispatch(fetchProductsSuccess(response.data))
    );
  } else {
    axios.get('/products').then(
      response => dispatch(fetchProductsSuccess(response.data))
    );
  }
};



export const getProduct = productId => dispatch => {
  axios.get(`/products/${productId}`)
    .then(res => dispatch(getProductSuccess(res.data)))
};

const getProductSuccess = product => {
  return {type: GET_PRODUCT_SUCCESS, product};
};

export const deleteProduct = (productId, token) => dispatch => {
  const headers = {"Token": token};

  axios.delete(`/products/${productId}`, {headers})
    .then(res => {
      dispatch(deleteProductSuccess());
      dispatch(push('/'));
    });
};

const deleteProductSuccess = () => {
  return {type: DELETE_PRODUCT_SUCCESS};
};



export const createProductSuccess = () => {
  return {type: CREATE_PRODUCT_SUCCESS};
};

export const createProduct = (productData, token) => dispatch => {
  const headers = {"Token": token};

  axios.post('/products', productData, {headers}).then(
    res => {
      dispatch(createProductSuccess());
      dispatch(push('/'));
    }
  )
};