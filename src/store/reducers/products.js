import {DELETE_PRODUCT_SUCCESS, FETCH_ALL_PRODUCTS_SUCCESS, GET_PRODUCT_SUCCESS} from "../actions/actionTypes";

const initialState = {
  products: [],
  lookingProduct: null
};

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case FETCH_ALL_PRODUCTS_SUCCESS:
      return {...state, products: action.products};
    case GET_PRODUCT_SUCCESS:
      return {...state, lookingProduct: action.product};
    case DELETE_PRODUCT_SUCCESS:
      return {...state, lookingProduct: null};
    default:
      return state;
  }
};

export default reducer;