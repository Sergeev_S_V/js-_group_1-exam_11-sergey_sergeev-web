import React, {Component} from 'react';
import {
  Button, Col, ControlLabel, Form, FormControl, FormGroup
} from "react-bootstrap";
import categories from '../../categories';

class ProductForm extends Component {

  state = {
    title: '',
    description: '',
    image: '',
    price: '',
    category: '',
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    formData.append("seller", this.props.user._id);

    this.props.onSubmit(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  firstLetterUppercase = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };

  renderCategories = categories => {
    return categories.map(category => (
      <option key={category} value={category}>
        {this.firstLetterUppercase(category)}
      </option>
    ));
  };

  render() {
    return(
        <Form horizontal onSubmit={this.submitFormHandler}>
          <FormGroup controlId="productTitle">
            <Col componentClass={ControlLabel} sm={2}>
              Title
            </Col>
            <Col sm={10}>
              <FormControl
                type="text"
                required
                placeholder="Enter title"
                name="title"
                value={this.state.title}
                onChange={this.inputChangeHandler}
              />
            </Col>
          </FormGroup>

          <FormGroup controlId="productDescription">
            <Col componentClass={ControlLabel} sm={2}>
              Description
            </Col>
            <Col sm={10}>
              <FormControl
                componentClass="textarea"
                required
                placeholder="Enter description"
                name="description"
                value={this.state.description}
                onChange={this.inputChangeHandler}
              />
            </Col>
          </FormGroup>

          <FormGroup controlId="productPrice">
            <Col componentClass={ControlLabel} sm={2}>
              Price
            </Col>
            <Col sm={10}>
              <FormControl
                type="number"
                required
                placeholder="Enter price"
                name="price"
                value={this.state.price}
                onChange={this.inputChangeHandler}
              />
            </Col>
          </FormGroup>

          <FormGroup controlId="productImage">
            <Col componentClass={ControlLabel} sm={2}>
              Image
            </Col>
            <Col sm={10}>
              <FormControl
                type="file"
                required
                name="image"
                onChange={this.fileChangeHandler}
              />
            </Col>
          </FormGroup>

          <FormGroup controlId="formControlsSelect">
            <Col componentClass={ControlLabel} sm={2}>
              <ControlLabel>Category</ControlLabel>
            </Col>
            <Col sm={10}>
              <FormControl componentClass="select" required
                           value={this.state.category}
                           name="category"
                           onChange={this.inputChangeHandler}
                           placeholder="Select category">
                {this.renderCategories(categories)}
              </FormControl>
            </Col>
          </FormGroup>

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button bsStyle="primary" type="submit">Create product</Button>
            </Col>
          </FormGroup>
        </Form>
    );
  }
}

export default ProductForm;