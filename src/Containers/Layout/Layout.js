import React, {Fragment} from 'react';
import {connect} from 'react-redux';

import Toolbar from "../../Components/UI/Toolbar/Toolbar";
import {logoutUser} from "../../store/actions/users";
import Categories from "../../Components/Sidebar/Categories/Categories";

const Layout = ({children, user, logoutUser}) => (
  <Fragment>
    <header>
      <Toolbar user={user} logout={logoutUser}/>
    </header>
    <main className="container">
      <Categories/>
      {children}
    </main>
  </Fragment>
);

const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(logoutUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(Layout);