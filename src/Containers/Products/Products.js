import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Col, PageHeader, Thumbnail} from "react-bootstrap";
import {fetchProducts} from "../../store/actions/products";
import config from "../../config";
import {LinkContainer} from "react-router-bootstrap";

class Products extends Component {
  componentDidMount() {
    const category = this.props.match.params.category;

    if (this.props.match.params.category) {
      this.props.onFetchProducts(category);
    } else {
      this.props.onFetchProducts();
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.category !== this.props.match.params.category) {
      const category = this.props.match.params.category;
      this.props.onFetchProducts(category);
    }
  };

  renderPosts = (products) => {
    const pathImage = config.apiUrl + '/uploads/';

    return products.map(product => (
      <LinkContainer key={product._id} to={`/product/${product._id}`}>
        <Col xs={6} md={4}>
          <Thumbnail src={pathImage + product.image} alt="242x200">
            <h3>{product.title}</h3>
            <p>{product.price} som</p>
          </Thumbnail>
        </Col>
      </LinkContainer>
    ));
  };

  render() {
    return (
      <Fragment>
        <PageHeader>
          Products
        </PageHeader>
        {this.props.products.length > 0
          ? this.renderPosts(this.props.products)
          : <p>Products are not exist</p>
        }
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    products: state.products.products,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchProducts: (category) => dispatch(fetchProducts(category)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);

