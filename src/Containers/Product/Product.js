import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, Image, PageHeader, Panel} from "react-bootstrap";
import config from "../../config";
import {deleteProduct, getProduct} from "../../store/actions/products";

class Product extends Component {
  componentDidMount() {
    const productId = this.props.match.params.id;
    this.props.onGetProduct(productId);
  }

  renderPosts = (product) => {
    const pathImage = config.apiUrl + '/uploads/';

    return(
      <Panel>
        <Panel.Heading style={{display: 'flex', justifyContent: 'space-between'}}>
          <span><b>Info about:</b> {product.title}</span>
          <span><b>Price:</b> {product.price}</span>
          <span><b>Category:</b> {product.category}</span>
        </Panel.Heading>
        <Panel.Body style={{display: 'flex', justifyContent: 'space-between'}}>
          <Image thumbnail src={pathImage + product.image}
                 style={{width: '300px', height: '200px'}}/>
          <Panel.Body><b>Description:</b> {product.description}</Panel.Body>
          <Panel.Body style={{display: 'flex', justifyContent: 'flex-start', flexDirection: 'column'}}>
            <Panel.Title><b>About seller</b></Panel.Title>
            <span><b>Name:</b> {product.seller.displayName}</span>
            <span><b>Phone:</b> {product.seller.phoneNumber}</span>
          </Panel.Body>
        </Panel.Body>
        {this.props.user && this.props.user._id === product.seller._id
          ? <Panel.Footer>
              <Button onClick={() => this.props.onDeleteProduct(product._id, this.props.user.token)}>
                Delete
              </Button>
            </Panel.Footer>
          : null
        }
      </Panel>
    );
  };

  render() {
    return (
      <Fragment>
        <PageHeader>Product info</PageHeader>
        {this.props.product
          ? this.renderPosts(this.props.product)
          : <p>No info</p>
        }
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    product: state.products.lookingProduct,
    user: state.users.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onGetProduct: productId => dispatch(getProduct(productId)),
    onDeleteProduct: (productId, token) => dispatch(deleteProduct(productId, token))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Product);

