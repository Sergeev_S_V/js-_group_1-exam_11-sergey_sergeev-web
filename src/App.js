import React, { Component } from 'react';
import {Route, Switch} from "react-router-dom";

import Layout from "./Containers/Layout/Layout";

import Register from "./Containers/Register/Register";
import Login from "./Containers/Login/Login";
import NewProduct from "./Components/NewProduct/NewProduct";
import Products from "./Containers/Products/Products";
import Product from "./Containers/Product/Product";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={Products}/>
          <Route path="/products/:category" exact component={Products}/>
          <Route path="/product/:id" exact component={Product}/>
          <Route path="/register" exact component={Register}/>
          <Route path="/login" exact component={Login}/>
          <Route path="/add_new_product" exact component={NewProduct}/>
        </Switch>
      </Layout>
    );
  }
}


// при создании товара если не заполнены все поля выводить ошибку
export default App;
